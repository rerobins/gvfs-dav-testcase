#!/usr/bin/env bash

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)
generated_files_directory="${script_dir}"/generated_files

mkdir -p "${generated_files_directory}"
cd "${generated_files_directory}"

for j in {1..5}
do
  mkdir -p "${generated_files_directory}"/${j}
  for i in {1..10000}
  do
  	echo ${i} > "${generated_files_directory}"/"${j}"/"${i}".txt
  done
done
