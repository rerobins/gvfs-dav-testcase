# gvfs-dav test case

## Installation

```bash
python -m venv env
source env/bin/activate
pip install -r requirements.txt
```

## Generate Test files

Generates 50,000 small text files (total size 1MB).

```bash
cd test_files
./generate.sh
```

## Execution

1. Start wsgidav server

```bash
env/bin/wsgidav --auth anonymous --root=./output_files 
```

2. Connect to the DAV Server in Nautilus `dav://localhost:8080/`

3. Copy the content of `test_files/generated_files` into the DAV server that has been mounted.

4. Wait for it to Hang. 

5. If it doesn't hang, Delete the files from the server and copy them again.  Or Try copying the test files into two different directories at the same time.

## Reset Instructions

Once it hangs, disconnect nautilus from the DAV server and reconnect.  Files can then be copied again showing that the WSGIDAV server is not the issue.

## Other Notes

Don't know if this is a gvfs-dav issue, but the progress bar seems hosed on my system.


